﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fractal : MonoBehaviour
{
    public Mesh mesh;
    public Material material;
    public int maxDepth;
    public float childScale;

    private int depth;

    private static Vector3[] childDir = {
        Vector3.up,
        Vector3.right,
        Vector3.left
    };

    private static Quaternion[] childOrient = {
        Quaternion.identity,
        Quaternion.Euler(0f, 0f, -90f),
        Quaternion.Euler(0f, 0f, 90f)
    };

    // Start is called before the first frame update
    void Start()
    {
        gameObject.AddComponent<MeshFilter>().mesh = mesh;
        gameObject.AddComponent<MeshRenderer>().material = material;

        if (depth < maxDepth)
        {
            StartCoroutine(CreateChildren());
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void Init(Fractal parent, int childIndex) {
        //Draw Object
        mesh = parent.mesh;
        material = parent.material;

        //Recursive Count
        maxDepth = parent.maxDepth;
        depth = parent.depth + 1;

        //Positioning and Scaling Child
        transform.parent = parent.transform;
        childScale = parent.childScale;
        transform.localScale = Vector3.one * childScale;
        transform.localPosition = childDir[childIndex] * (0.5f + 0.5f * childScale);
        transform.localRotation = childOrient[childIndex];
    }

    IEnumerator CreateChildren() {

        for (int i = 0; i < childDir.Length; i++)
        {
            yield return new WaitForSeconds(Random.Range(0.1f, 0.5f));
            new GameObject("Fractal Child")
                .AddComponent<Fractal>().Init(this, i);
        }        
    }

}
